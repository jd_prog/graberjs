
var exp =  async function () {

    return { data: [
        {title: 'Default event one',
        participantsLimit: '1',
        dateStart: 1543678150308,
        dateEnd: 1543678150308,
        creator: { username: 'id', displayName: "user"},
        status: 'PUBLISH',
        description: 'Default event one'},
        {
            title: 'Default event two',
            participantsLimit: '1',
            dateStart: 1543678150308,
            dateEnd: 1543678150308,
            creator: { username: 'id', displayName: "user"},
            status: 'PUBLISH',
            description: 'Default event two'}
        ]
    }

};


exports.exp = exp;