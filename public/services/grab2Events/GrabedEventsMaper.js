
var PuppeterLauncher = require('./PuppeterLauncher');

class GrabedEventsMaper {

    constructor(_googleMapsClient) {
        this.googleMapsClient = _googleMapsClient;
    }

    async mapGrabedEvents(grabedData, _googleMapsClient) {
        let googleMapsClient = _googleMapsClient;
        let promisesArray = await getFormatedLocation(grabedData);
        let array = await getArray(promisesArray);
        this.mapedEvents = mapGrabedEventWithLocation(array);

        async function getFormatedLocation(grabedData) {
            let promisesArray = [];
            promisesArray.push(Promise.resolve(grabedData));
            for (let i = 0; i < grabedData.data.length; i++) {
                promisesArray.push(await getLocation(grabedData.data[i].location));
            }
            return promisesArray;
        }

        async function getLocation(locationText) {
            if(locationText === ''){
                return locationText;
            }else {
                return googleMapsClient.geocode({address: locationText}, function (err, response) {
                    let location = {};
                    let obj = response.json.results;
                    location['formattedAddress'] = obj[0].formatted_address;
                    location['placeId'] = obj[0].place_id;
                    location['latitude'] = obj[0].geometry.location.lat;
                    location['longitude'] = obj[0].geometry.location.lng;
                    obj[0].address_components.forEach(it => {
                        if (it.types[0] === 'locality' && it.types[1] === 'political') {
                            location['cityName'] = it.short_name;
                        }
                    });
                    return location;
                }).asPromise();
            }}
        async function getArray(arrayOfActions) {
            return await Promise.all(arrayOfActions);
        }
        function mapGrabedEventWithLocation(eventsLocation) {
            for (let i = 1; i < eventsLocation.length; i++) {
                if(eventsLocation[i].json !== undefined) {
                    let location = {};
                    let obj = eventsLocation[i].json.results;
                    location['formattedAddress'] = obj[0].formatted_address;
                    location['placeId'] = obj[0].place_id;
                    location['latitude'] = obj[0].geometry.location.lat;
                    location['longitude'] = obj[0].geometry.location.lng;
                    obj[0].address_components.forEach(it1 => {
                        if (it1.types[0] === 'locality') {
                            location['cityName'] = it1.short_name;
                        }
                    });
                    eventsLocation[0].data[i - 1].location = location;
                } else {
                    let location = {};
                    eventsLocation[0].data[i - 1].location = location;
                }
            }
            return eventsLocation[0];
        }
    }
    getMappedEvents() { return this.mapedEvents; }
}

module.exports = GrabedEventsMaper;