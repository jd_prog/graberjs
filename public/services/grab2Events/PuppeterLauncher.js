const puppeteer = require('puppeteer');
const config = require('./resources/graber-config');

class PuppeterLauncher {

    constructor(puppeteer) {
        return async () => {
            this.browser = await puppeteer.launch({headless: false});
            return this;
        };

    }

  async init(browser) {

        let page  = await grabe2EventsMainPageInitialization();
        let pageHandler = await getPageHandeler(page);
        let eventDescriptionAndLocation = await getEventDescriptionAndLocation(page, pageHandler);
        this.grabedData = await getPageEvaluationResult(page, eventDescriptionAndLocation);

        async function grabe2EventsMainPageInitialization()
       {
           let page = await browser.newPage();
           await page.setViewport({width: 1920, height: 1080});
           await page.goto(config['pageName']);
           await page.waitFor(3000);
           await page.select('.category-search', config['topic']);
           await page.waitForSelector('body > div.event-search-panel > div > form > div > div.search-items > div.col-md-1.col-xs-12 > button');
           await page.click('body > div.event-search-panel > div > form > div > div.search-items > div.col-md-1.col-xs-12 > button');
           await page.waitForSelector('.single-event');
           return page;
       }

      async function getPageHandeler(page)
       {
           let pageHandler = await
           page.$$('.event-poster');
           return pageHandler;
       }
       async function getEventDescriptionAndLocation(page, pageHandler)
       {
           let eventDescription = [];
           for (let i = 1; i < pageHandler.length + 1; i++) {
               await page.click(`body > div.container.text-center > div.row.events-list > div:nth-child(${i}) > div > a`);
               await page.waitForSelector('.box-hide');
               await page.waitFor(2000);
               const description = await page.evaluate(() => {
                   let description = $($('.box-hide')[0]).text();
                   let location = $('.event-address-street').text().trim();
                   let title = $('.location-title').text().trim();
                   let participantsLimit = $($('[title=Відвідувачі]')[0]).text().trim();
                   if (description.length > 998) {
                       description = description.substring(0, 998);
                   }
                   return {
                       title,
                       description,
                       location,
                       participantsLimit
                   };
               }).then(resolver => {
                   eventDescription.push(resolver);
               });
               await page.waitFor(2000);
               await page.goBack();
           }
           return eventDescription;
       }
       async function getPageEvaluationResult(page, eventDescriptions)
       {
           let pageEvaluationResult = await
           page.evaluate(async (eventDescriptions) => {

               let events = $('.single-event');
               let parsedEvents = parseEvents(events);
               parsedEvents = setEventDescriptionAndLocation(parsedEvents, eventDescriptions);
               return {
                   data: parsedEvents
               };

               function parseEvents(events) {
                   let grabedEvents = [];
                   for (let i = 0; i < events.length; i++) {
                       let title = $($(events).find('.event-name')[i]).text().trim();
                       let date = $($($('.single-event')[i]).find('.event-location-item')[1]).text();
                       let image = $($(events).find('.event-poster')).css('background-image');
                       let creator = {username: 'id', displayName: "user"};
                       let status = 'PUBLISH';
                       let dates = parseDate(date);
                       let dateStart = new Date().setTime(dates['dateStart'].toDate().getTime());
                       let dateEnd = new Date().setTime(dates['dateEnd'].toDate().getTime());
                       grabedEvents.push({title, dateStart, dateEnd, creator, status});
                   }
                   return grabedEvents;
               }

               function setEventDescriptionAndLocation(events, eventDescriptions) {
                   for (let i = 0; i < events.length; i++) {
                       for (let j = 0; j < eventDescriptions.length; j++) {
                           if (events[i].title === eventDescriptions[j].title) {
                               events[i].location = eventDescriptions[j].location;
                               events[i].description = eventDescriptions[j].description;
                               events[i].participantsLimit = eventDescriptions[j].participantsLimit;
                           }
                       }
                   }
                   return events;
               }

               function parseDate(date) {
                    var obj = {};
                   if(date.includes('-') && date.indexOf('-') === 3) {
                       let dates = date.split('-');
                       dates[0] = dates[0].trim();
                       dates[1] = dates[1].trim();
                       dates[0] = dates[0]+ dates[1].substring(2);
                       obj.dateStart = moment(dates[0],"DD/MM/YYYY");
                       obj.dateEnd = moment(dates[1], "DD/MM/YYYY");
                   } else if(date.includes('-') && date.indexOf('-') === 6) {
                       let dates = date.split('-');
                       dates[0] = dates[0].trim();
                       dates[1] = dates[1].trim();
                       dates[0] = dates[0] + (dates[1].substring(5));
                       obj.dateStart = moment(dates[0], "DD/MM/YYYY");
                       obj.dateEnd = moment(dates[1], "DD/MM/YYYY");
                   }
                   if(date.indexOf('-') > 11){
                       date = date.substring(0, 10);
                       obj.dateStart = moment(date, "DD/MM/YYYY");
                       obj.dateEnd = moment(date, "DD/MM/YYYY");
                   }
                   return obj;
               }
           }, eventDescriptions);
           return pageEvaluationResult;
       }
   }

    close(){
        this.browser.close();
    }

}

module.exports = PuppeterLauncher;