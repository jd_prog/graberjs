const config = require('./resources/graber-config');
const puppeteer = require('puppeteer');
const googleMapsClient = require('@google/maps');
var PuppeterLauncher = require('./PuppeterLauncher');
var GrabedEventsMaper = require('./GrabedEventsMaper');

async function exp() {
    let graber = await new PuppeterLauncher(puppeteer)();
    await graber.init(graber.browser);

    let client = googleMapsClient.createClient({
        key: 'AIzaSyA_Zfegl8_ZNInJjBv3WuSPjBOn4J2g8cE',
        Promise: Promise
    });

    let grabedEventMapper = await new GrabedEventsMaper(client);

    await grabedEventMapper.mapGrabedEvents(graber.grabedData, client);
    graber.close();
    return grabedEventMapper.getMappedEvents();
}


exports.exp = exp;
