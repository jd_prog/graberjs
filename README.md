
#Graber js

This is a simple node js events graber.


##Installation
download the source files. 

Then run 
```bash  
npm install
``` 

after that run 

```bash 
npm link
```

if your os require root user permition , than run 

```bash 
sudo npm link
```

## Usage

```bash 
graber --version
``` 
- returns current graber version

```bash 
graber grab -h <hostname> -p <port> -s <service>
``` 
- run graber service whose configuration and source code is in _`public/service/<services>`_ folder


```bash 
graber start -h <hostname> -p <port> 
``` 
- run all graber services that is located in _`public/services`_ folder 


```bash 
graber create -n <name>  
``` 
- will create graber service in _`public/services/<name>`_ directory with file name `<name>.js` 
( this file return Promise with grabed events .)
  and link this file with graber name in _`config/services.json`_ 
 
- like that 
    
    ```json
     {
     
     <name> : <path to the *.js file where Promise is returned>
     
     }
    ```
 
Also _`public/services/<name>/resources`_ will be created , where all configs and resources will be stored



```bash 
graber rm -n <name>  
``` 
- will remove graber service from project


Test if your custom service is working with typing 
```bash 
graber grab -h <hostname> -p <port> -s <name>
``` 
