#!/usr/bin/env node

var postData = require('./bin/post-data');
var service = require('./service');
const program = require('commander');
var services = require('./config/services.json');
var fs = require('fs');
var mkdirp = require('mkdirp');
var rimraf = require('rimraf');

program
    .version('1.0.0')
    .description('Client Managment System')

program
    .command('grab')
    .option('-h, --host <host>', 'host name')
    .option('-p, --port <port>', 'port name')
    .option('-s, --service <service>', 'service name')
    .option('-f, --file [file]', 'page name')
    .option('-c, --category [category]','category name')
    .alias('g')
    .description('start a graber with specific options')
    .action( (options) => {
        if( options['host'] !== undefined || options['port']!== undefined || options['service'] !==undefined) {
            host = options['host'];
            port = options['port'];
            serviceName = options['service'];
            pageName = (options['file'] === undefined ) ? '' :options['file'];
            category = (options['category'] === undefined ) ?  'https://2event.com/uk/events/all' : options['category'];
            service.service(services[`${serviceName}`] ,host, port, pageName, category).then( results => {
                console.log(results);
                postData.postData(results, port, host);

            });
        } else {
            console.log('no valid argument');
        }
    });

program
    .command('start')
    .option('-h, --host <host>', 'host name')
    .option('-p, --port <port>', 'port name')
    .description("start up all services for gravinf events")
    .action( (options) => {
        if( options['host'] !== undefined || options['port']!== undefined ) {
            host = options['host'];
            port = options['port'];
            var obj = JSON.parse(fs.readFileSync('config/services.json', 'utf8'));
            Object.keys(obj).forEach( key => {
                service.service(services[key], host, port, 'https://2event.com/uk/events/all', '').then( results => {
                    postData.postData(results, port, host);
                });
            });
        }
    });

program
    .command('create')
    .option('-n, --name <name>', 'service name')
    .description("creating simple graber")
    .action( async (options) => {
        if(options['name'] !== undefined){
            serviceName = options['name'];
            await mkdirp(`./public/services/${serviceName}`, function (err) {});
            await mkdirp(`./public/services/${serviceName}/resources`, function (err) {});
            let path = `./public/services/${serviceName}/${serviceName}.js`;
            let pathConfig = `./public/services/${serviceName}/resources/graber-config.js`;

            fs.writeFile(path, 'var exp = async function fun() {\n' +
                '    return { data: []};\n' +
                '};'+'\n'+
                '\n' +
                'exports.exp = exp;', { flag: 'wx' }, function (err) {});

            fs.writeFile(pathConfig, 'module.exports = { \n'+
                                    '};', {flag: 'wx',}, function (err) {});


            fs.readFile('config/services.json', async function (err, data) {
               var json = JSON.parse(data);
               json[`${serviceName}`]= path;
               fs.writeFile('config/services.json', JSON.stringify(json), (err) => {});
            });
        }
        else {
            console.log('not a valid service name');
        }
    });
program
    .command('rm')
    .option('-n, --name <name>', 'service name')
    .description("remove existing graber")
    .action(async (options) => {
       if(options['name'] !== undefined) {
           serviceName = options['name'];
           let path = `./public/services/${serviceName}`;
           rimraf(path, function () {});
           fs.readFile('config/services.json', async function (err, data) {
               var json = JSON.parse(data);
               delete json[`${serviceName}`];
               fs.writeFile('config/services.json', JSON.stringify(json), (err) => {});
           });
           console.log(`service ${serviceName} was removed` );
       }else {
           console.log('no such service')
       }
    });



program.parse(process.argv);