#!/usr/bin/env node

var grabejs = require('../public/services/grab2Events/grabe2Events');

var querystring = require('querystring');
var http = require('http');
var fs = require('fs');


function PostCode(grabedData, port, host,) {

    var requestUser = {
        username: 'id',
        password: 'pAmSzlSs',
        grant_type: 'password'
    };

    var post_data = querystring.stringify(requestUser);

    var post_options = {
        host: host,
        port: port,
        path: '/oauth/token',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Basic Y29uZmFzc2lzdGFudDpzZWNyZXQ=',
            "Content-Length": Buffer.byteLength(post_data)
        }
    };

    var post_grabed_options = {
        host: host,
        port: port,
        path: '/api/events/grabed',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        }
    };
    var post_req = http.request(post_options, function(res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            access_token = JSON.parse(chunk).access_token;
            post_grabed_options.headers['Authorization'] = `bearer ${access_token}`;
            var post_grabed_data = http.request(post_grabed_options, function (resp) {
                resp.setEncoding('utf8');
                resp.on('data', function (grabedEvents) {
                });
            });

            post_grabed_data.write(JSON.stringify(grabedData.data));
            post_grabed_data.end();
        });
    });
    post_req.write(post_data);
    post_req.end();
}

exports.postData = PostCode;
